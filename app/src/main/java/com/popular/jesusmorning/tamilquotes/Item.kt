package com.popular.jesusmorning.tamilquotes

 data class Item(
         val topic_title:String = "Topic Title",
         val topic_icon:String = "",
         val menus:ArrayList<String>
  )
